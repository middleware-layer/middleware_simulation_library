/**
 ******************************************************************************
 * @file    GPSSimulator.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2017
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/GPSSimulator/Inc/GPSSimulator_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace Simulation
{
   static I_GPS_Simulator* p_GPS_Simulator_Impl = NULL;

   const char* GPS_Simulator_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle GPS_Simulator functions";
   }

   void stub_setImpl(I_GPS_Simulator* pNewImpl)
   {
      p_GPS_Simulator_Impl = pNewImpl;
   }

   static I_GPS_Simulator* GPS_Simulator_Stub_Get_Impl(void)
   {
      if(p_GPS_Simulator_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to GPS_Simulator functions. Did you forget"
                   << "to call Stubs::Simulation::stub_setImpl() ?" << std::endl;

         throw GPS_Simulator_StubImplNotSetException();
      }

      return p_GPS_Simulator_Impl;
   }

      ////////////////////////////////////////
      // I_GPS_Simulator Methods Definition //
      ////////////////////////////////////////

   I_GPS_Simulator::~I_GPS_Simulator()
   {
      if(p_GPS_Simulator_Impl == this)
      {
         p_GPS_Simulator_Impl = NULL;
      }
   }

   ////////////////////////////////////////////
   // _Mock_GPS_Simulator Methods Definition //
   ////////////////////////////////////////////

   _Mock_GPS_Simulator::_Mock_GPS_Simulator()
   {

   }

               //////////////////////////////////
               // Definition of Non Stub Class //
               //////////////////////////////////

   GPS_Simulator::GPS_Simulator()
   {
   }

   GPS_Simulator::~GPS_Simulator()
   {
   }
}
