/**
 ******************************************************************************
 * @file    GPSSimulator_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2017
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_GPSSIMULATOR_INC_GPSSIMULATOR_STUB_HPP
#define SOURCE_TESTING_STUBS_GPSSIMULATOR_INC_GPSSIMULATOR_STUB_HPP

#include <Production/GPSSimulator/Inc/GPSSimulator.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Simulation
{
   class GPS_Simulator_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_GPS_Simulator
   {
      public:
         virtual ~I_GPS_Simulator();
   };

   class _Mock_GPS_Simulator : public I_GPS_Simulator
   {
      public:
         _Mock_GPS_Simulator();
   };

   typedef ::testing::NiceMock<_Mock_GPS_Simulator> Mock_GPS_Simulator;

   void stub_setImpl(I_GPS_Simulator* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_GPSSIMULATOR_INC_GPSSIMULATOR_STUB_HPP
