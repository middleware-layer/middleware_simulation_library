#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: Midleware_Simulation_Library                                        #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

coloredMessage(BoldYellow "Loading Stub GPSSimulator CMakeLists" STATUS)

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
# C. Invoke Subdirectories                  #
# D. Libraries / Executables                #
#############################################

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
#############################################

set(DIRECTORY_NAME_SRC "${PROJECT_STUB_TESTING_SOURCE_FOLDER}/GPSSimulator/Src")
set(DIRECTORY_NAME_INC "${PROJECT_STUB_TESTING_SOURCE_FOLDER}/GPSSimulator/Inc")

#############################################
#   A.1. Set Options                        #
#############################################

set(Stubs_GPSSimulator_Sources  ${DIRECTORY_NAME_SRC}/GPSSimulator_stub.cpp
                                PARENT_SCOPE)

set(Stubs_GPSSimulator_Headers  ${DIRECTORY_NAME_INC}/GPSSimulator_stub.hpp
                                PARENT_SCOPE)

#############################################
#   A.2. Use Options                        #
#############################################

#############################################
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
#############################################

#############################################
#   B.1. Required libraries                 #
#############################################

#############################################
#   B.2. Optional libraries                 #
#############################################

#############################################
#   B.3. Development and debug libraries    #
#############################################

#############################################
# C. Invoke Subdirectories                  #
#############################################

#############################################
# D. Libraries / Executables                #
#############################################

