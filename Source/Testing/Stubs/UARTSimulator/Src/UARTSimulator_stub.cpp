/**
 ******************************************************************************
 * @file    UARTSimulator.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2017
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/UARTSimulator/Inc/UARTSimulator_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace Simulation
{
   static I_UART_Simulator* p_UART_Simulator_Impl = NULL;

   const char* UART_Simulator_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle UART_Simulator functions";
   }

   void stub_setImpl(I_UART_Simulator* pNewImpl)
   {
      p_UART_Simulator_Impl = pNewImpl;
   }

   static I_UART_Simulator* UART_Simulator_Stub_Get_Impl(void)
   {
      if(p_UART_Simulator_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to UART_Simulator functions. Did you forget"
                   << "to call Stubs::Simulation::stub_setImpl() ?" << std::endl;

         throw UART_Simulator_StubImplNotSetException();
      }

      return p_UART_Simulator_Impl;
   }

      ////////////////////////////////////////
      // I_UART_Simulator Methods Definition //
      ////////////////////////////////////////

   I_UART_Simulator::~I_UART_Simulator()
   {
      if(p_UART_Simulator_Impl == this)
      {
         p_UART_Simulator_Impl = NULL;
      }
   }

   ////////////////////////////////////////////
   // _Mock_UART_Simulator Methods Definition //
   ////////////////////////////////////////////

   _Mock_UART_Simulator::_Mock_UART_Simulator()
   {

   }

               //////////////////////////////////
               // Definition of Non Stub Class //
               //////////////////////////////////

   UART_Simulator::UART_Simulator()
   {
   }

   UART_Simulator::~UART_Simulator()
   {
   }
}
