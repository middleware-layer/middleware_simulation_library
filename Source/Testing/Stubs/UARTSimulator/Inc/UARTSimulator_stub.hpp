/**
 ******************************************************************************
 * @file    UARTSimulator_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2017
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_UARTSIMULATOR_INC_UARTSIMULATOR_STUB_HPP
#define SOURCE_TESTING_STUBS_UARTSIMULATOR_INC_UARTSIMULATOR_STUB_HPP

#include <Production/UARTSimulator/Inc/UARTSimulator.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Simulation
{
   class UART_Simulator_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_UART_Simulator
   {
      public:
         virtual ~I_UART_Simulator();
   };

   class _Mock_UART_Simulator : public I_UART_Simulator
   {
      public:
         _Mock_UART_Simulator();
   };

   typedef ::testing::NiceMock<_Mock_UART_Simulator> Mock_UART_Simulator;

   void stub_setImpl(I_UART_Simulator* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_UARTSIMULATOR_INC_UARTSIMULATOR_STUB_HPP
