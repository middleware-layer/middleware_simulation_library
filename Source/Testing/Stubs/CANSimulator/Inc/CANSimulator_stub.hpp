/**
 ******************************************************************************
 * @file    CANSimulator_stub.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2017
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_TESTING_STUBS_CANSIMULATOR_INC_CANSIMULATOR_STUB_HPP
#define SOURCE_TESTING_STUBS_CANSIMULATOR_INC_CANSIMULATOR_STUB_HPP

#include <Production/CANSimulator/Inc/CANSimulator.hpp>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <exception>

namespace Simulation
{
   class CAN_Simulator_StubImplNotSetException : public std::exception
   {
      public:
         virtual const char* what(void) const throw ();
   };

   class I_CAN_Simulator
   {
      public:
         virtual ~I_CAN_Simulator();
   };

   class _Mock_CAN_Simulator : public I_CAN_Simulator
   {
      public:
         _Mock_CAN_Simulator();
   };

   typedef ::testing::NiceMock<_Mock_CAN_Simulator> Mock_CAN_Simulator;

   void stub_setImpl(I_CAN_Simulator* pNewImpl);
}

#endif // SOURCE_TESTING_STUBS_CANSIMULATOR_INC_CANSIMULATOR_STUB_HPP
