/**
 ******************************************************************************
 * @file    CANSimulator_stub.cpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2017
 * @brief
 ******************************************************************************
 */

#include <Testing/Stubs/CANSimulator/Inc/CANSimulator_stub.hpp>

#include <iostream>

using ::testing::_;
using ::testing::Invoke;

                        //////////////////////
                        // Mocks themselves //
                        //////////////////////

namespace Simulation
{
   static I_CAN_Simulator* p_CAN_Simulator_Impl = NULL;

   const char* CAN_Simulator_StubImplNotSetException::what(void) const throw()
   {
      return "No stub implementation is set to handle CAN_Simulator functions";
   }

   void stub_setImpl(I_CAN_Simulator* pNewImpl)
   {
      p_CAN_Simulator_Impl = pNewImpl;
   }

   static I_CAN_Simulator* CAN_Simulator_Stub_Get_Impl(void)
   {
      if(p_CAN_Simulator_Impl == NULL)
      {
         std::cerr << "ERROR: No Stub implementation is currently set to handle "
                   << "calls to CAN_Simulator functions. Did you forget"
                   << "to call Stubs::Simulation::stub_setImpl() ?" << std::endl;

         throw CAN_Simulator_StubImplNotSetException();
      }

      return p_CAN_Simulator_Impl;
   }

      ////////////////////////////////////////
      // I_CAN_Simulator Methods Definition //
      ////////////////////////////////////////

   I_CAN_Simulator::~I_CAN_Simulator()
   {
      if(p_CAN_Simulator_Impl == this)
      {
         p_CAN_Simulator_Impl = NULL;
      }
   }

   ////////////////////////////////////////////
   // _Mock_CAN_Simulator Methods Definition //
   ////////////////////////////////////////////

   _Mock_CAN_Simulator::_Mock_CAN_Simulator()
   {

   }

               //////////////////////////////////
               // Definition of Non Stub Class //
               //////////////////////////////////

   CAN_Simulator::CAN_Simulator()
   {
   }

   CAN_Simulator::~CAN_Simulator()
   {
   }
}
