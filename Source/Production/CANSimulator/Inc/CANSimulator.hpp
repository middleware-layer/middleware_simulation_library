/**
 ******************************************************************************
 * @file    CANSimulator.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2017
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_CANSIMULATOR_INC_CANSIMULATOR_HPP
#define SOURCE_PRODUCTION_CANSIMULATOR_INC_CANSIMULATOR_HPP

namespace Simulation
{
   class CAN_Simulator
   {
      private:

      protected:

      public:
         /**
          * @brief
          */
         CAN_Simulator();

         /**
          * @brief
          */
         virtual ~CAN_Simulator();
   };
}

#endif // SOURCE_PRODUCTION_CANSIMULATOR_INC_CANSIMULATOR_HPP
