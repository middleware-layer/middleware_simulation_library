/**
 ******************************************************************************
 * @file    GPSSimulator.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2017
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_GPSSIMULATOR_INC_GPSSIMULATOR_HPP
#define SOURCE_PRODUCTION_GPSSIMULATOR_INC_GPSSIMULATOR_HPP


namespace Simulation
{
   class GPS_Simulator
   {
      private:

      protected:

      public:
         /**
          * @brief
          */
         GPS_Simulator();

         /**
          * @brief
          */
         virtual ~GPS_Simulator();
   };
}

#endif // SOURCE_PRODUCTION_GPSSIMULATOR_INC_GPSSIMULATOR_HPP
