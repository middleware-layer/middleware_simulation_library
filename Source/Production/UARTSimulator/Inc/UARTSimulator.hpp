/**
 ******************************************************************************
 * @file    UARTSimulator.hpp
 * @author  leonardo.pereira
 * @version v1.0
 * @date    14 de dez de 2017
 * @brief
 ******************************************************************************
 */

#ifndef SOURCE_PRODUCTION_UARTSIMULATOR_INC_UARTSIMULATOR_HPP
#define SOURCE_PRODUCTION_UARTSIMULATOR_INC_UARTSIMULATOR_HPP

namespace Simulation
{
   class UART_Simulator
   {
      private:

      protected:

      public:
         /**
          * @brief
          */
         UART_Simulator();

         /**
          * @brief
          */
         virtual ~UART_Simulator();
   };
}

#endif // SOURCE_PRODUCTION_UARTSIMULATOR_INC_UARTSIMULATOR_HPP
