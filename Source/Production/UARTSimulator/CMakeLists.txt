#####################################################################################
# File: CMakeLists.txt                                                              #
#                                                                                   #
# Project Name: Midleware_Simulation_Library                                        #
#                                                                                   #
# Author: Leonardo Winter Pereira (leonardowinterpereira@gmail.com)                 #
#                                                                                   #
#####################################################################################

coloredMessage(BoldYellow "Loading UARTSimulator CMakeLists" STATUS)

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
# C. Invoke Subdirectories                  #
# D. Libraries / Executables                #
#############################################

#############################################
# A. Options                                #
#   A.1. Set Options                        #
#   A.2. Use Options                        #
#############################################

set(DIRECTORY_NAME_SRC "${PROJECT_PRODUCTION_SOURCE_FOLDER}/UARTSimulator/Src")
set(DIRECTORY_NAME_INC "${PROJECT_PRODUCTION_SOURCE_FOLDER}/UARTSimulator/Inc")

#############################################
#   A.1. Set Options                        #
#############################################

set(UARTSimulator_Sources   ${DIRECTORY_NAME_SRC}/UARTSimulator.cpp
                            PARENT_SCOPE)

set(UARTSimulator_Headers   ${DIRECTORY_NAME_INC}/UARTSimulator.hpp
                            PARENT_SCOPE)

#############################################
#   A.2. Use Options                        #
#############################################

#############################################
# B. Find System tools and libraries        #
#   B.1. Required libraries                 #
#   B.2. Optional libraries                 #
#   B.3. Development and debug libraries    #
#############################################

#############################################
#   B.1. Required libraries                 #
#############################################

#############################################
#   B.2. Optional libraries                 #
#############################################

#############################################
#   B.3. Development and debug libraries    #
#############################################

#############################################
# C. Invoke Subdirectories                  #
#############################################

#############################################
# D. Libraries / Executables                #
#############################################

